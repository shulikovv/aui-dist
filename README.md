# aui-dist

The Atlassian User Interface (AUI) library release distributions.

This repository only contains auto-generated release bundles of the [AUI](https://docs.atlassian.com/aui/) library. See https://bitbucket.org/atlassian/aui/ for the full source code.

## License

AUI is released under the [Apache 2 license](https://bitbucket.org/atlassian/aui/src/master/LICENSE).
See the [licenses directory](https://bitbucket.org/atlassian/aui/src/master/licenses/) for information about AUI and included libraries.
